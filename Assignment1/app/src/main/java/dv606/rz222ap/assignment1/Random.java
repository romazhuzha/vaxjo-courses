package dv606.rz222ap.assignment1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Random extends Activity {
    // needful outlines
    private static TextView randomNumberText;

    private static final String RANDOM_NUMBER_KEY = "randomNumber";

    private static Integer randomNumber;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random);

        randomNumberText = (TextView) findViewById(R.id.randomNumberText);

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            randomNumber = savedInstanceState.getInt(RANDOM_NUMBER_KEY);
        } else {
            // Initialize members with default values for a new instance
            randomNumber = getResources().getInteger(R.integer.RND_RANDOM_NUMBER);
        }
        randomNumberText.setText(Integer.toString(randomNumber));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(RANDOM_NUMBER_KEY, randomNumber);
        super.onSaveInstanceState(outState);
    }

    // Changes the number to the random one
    public void getRandom(View view) {
        java.util.Random rand = new java.util.Random();
        randomNumber = rand.nextInt(100) + 1;

        randomNumberText.setText(Integer.toString(randomNumber));
    }
}
