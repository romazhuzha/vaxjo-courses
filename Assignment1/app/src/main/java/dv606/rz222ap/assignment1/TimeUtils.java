/**
 * TimeUtils.java
 * Created: May 9, 2010
 * Jonas Lundberg, LnU
 * Edited: Sep 26, 2012
 * Roman Zhuzha, ONPU
 */
package dv606.rz222ap.assignment1;

import android.text.format.Time;

public class TimeUtils {

    public static Time getTime(String date_string) {
        Time time = new Time();
        try {
            time.parse3339(date_string);
            // System.out.println(date_string);
            // System.out.println(time);
            // System.out.println(getYYMMDD(time)+" "+getHHMM(time));
        } catch (Exception e) {
            System.out.println("Time string: " + date_string);
            e.printStackTrace();
        }
        return time;
    }

    public static String getYYMMDD(Time time) {
        // E.x. 13/09/2014
        return addZero(time.monthDay) + "/" + addZero(time.month) + "/" + time.year;
    }

    public static String getHHMM(Time time) {
        return addZero(time.hour) + ":" + addZero(time.minute);
    }

    private static String addZero(int n) {
        if (n < 10)
            return "0" + n;
        else
            return Integer.toString(n);
    }
}
