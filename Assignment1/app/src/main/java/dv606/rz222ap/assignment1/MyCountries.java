package dv606.rz222ap.assignment1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class MyCountries extends Activity {
    // Map attributes names
    private static final String ATTRIBUTE_YEAR_TEXT = "year";
    private static final String ATTRIBUTE_COUNTRY_TEXT = "country";

    // Key for saving instance state
    private static final String DATA = "data";

    // request code for second activity
    private static final int REQUEST_CODE = 0;

    // data structure for adapter
    private static ArrayList<Map<String, Object>> data;
    private static SimpleAdapter sAdapter;

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_my_countries);

        ListView simpleList = (ListView) findViewById(R.id.myCountriesListView);

        // array of attributes names, from which data will be read
        String[] from = { ATTRIBUTE_YEAR_TEXT, ATTRIBUTE_COUNTRY_TEXT };
        // array of id's, in which data will be add
        int[] to = { R.id.itemYear, R.id.itemCountry };

        if (savedInstanceState != null) {
            data = (ArrayList<Map<String, Object>>) savedInstanceState.getSerializable(DATA);
        } else {
            data = new ArrayList<Map<String, Object>>();
        }
        // creating adapter
        sAdapter = new SimpleAdapter(this, data, R.layout.list_item_countries, from, to);

        // list defining and setting it an adapter
        simpleList.setAdapter(sAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_my_countries, menu);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(DATA, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.itemAdd:
                Intent intent = new Intent(this, AddInformation.class);
                startActivityForResult(intent, REQUEST_CODE);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        AddInformation addInformClass = new AddInformation();
        if (requestCode == REQUEST_CODE) {
            switch (resultCode) {
                case RESULT_OK:
                    String yearExtra = result.getStringExtra(addInformClass.getExtraMessageYear());
                    String countryExtra = result.getStringExtra(addInformClass.getExtraMessageCountry());

                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put(ATTRIBUTE_YEAR_TEXT, yearExtra);
                    map.put(ATTRIBUTE_COUNTRY_TEXT, countryExtra);
                    data.add(map);

                    // notify about data changing
                    sAdapter.notifyDataSetChanged();
                    Toast.makeText(this, R.string.MC_ADD_OK, Toast.LENGTH_SHORT).show();
                    break;
                case RESULT_CANCELED:
                    Toast.makeText(this, R.string.MC_ADD_CANCEL, Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(this, R.string.MC_ADD_ERROR, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }
}
