/**
 * VaxjoWeather.java Created: May 9, 2010 Jonas Lundberg, LnU Edited: Sep 9, 2014 Roman Zhuzha,
 * ONPU
 */

package dv606.rz222ap.assignment1;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

/**
 * This is a first prototype for a weather app. It is currently only downloading
 * weather data for Vaxjo.
 * <p>
 * This activity downloads weather data and constructs a WeatherReport, a data
 * structure containing weather data for a number of periods ahead.
 * <p>
 * The WeatherHandler is a SAX parser for the weather reports (forecast.xml)
 * produced by www.yr.no. The handler constructs a WeatherReport containing meta
 * data for a given location (e.g. city, country, last updated, next update) and
 * a sequence of WeatherForecasts. Each WeatherForecast represents a forecast
 * (weather, rain, wind, etc) for a given time period.
 * <p>
 * The next task is to construct a list based GUI where each row displays the
 * weather data for a single period.
 *
 * @author jonasl
 */

public class VaxjoWeather extends Activity {
    static final String FORECAST_URL = "http://www.yr.no/sted/Sverige/Kronoberg/V%E4xj%F6/forecast.xml";
    // http://www.yr.no/sted/Sverige/Blekinge/Karlskrona/forecast.xml
    // http://www.yr.no/sted/Sverige/Kronoberg/V%E4xj%F6/forecast.xml
    // data structure for adapter
    static ArrayList<Map<String, Object>> data;
    static Map<String, Object> map;
    static ListView lvSimple;
    static SimpleAdapter sAdapter;

    // Map attributes names
    static final String ATTRIBUTE_NAME_DATE_TEXT = "date_text";
    static final String ATTRIBUTE_NAME_PERIOD_TEXT = "period_text";
    static final String ATTRIBUTE_NAME_TEMP_TEXT = "temp_text";
    static final String ATTRIBUTE_NAME_WEATHER_IMAGE = "weather_image";

    // forecast parts
    static String dateText, periodText, temperatureText;
    static int periodCode, weatherCode;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather_main);

        data = new ArrayList<Map<String, Object>>();

        // attributes names array to read data from
        String[] from = {ATTRIBUTE_NAME_DATE_TEXT, ATTRIBUTE_NAME_PERIOD_TEXT, ATTRIBUTE_NAME_TEMP_TEXT,
                ATTRIBUTE_NAME_WEATHER_IMAGE};

        // id components to input data
        int[] to = {R.id.date_text, R.id.period_text, R.id.temp_text, R.id.weather_image};

        // creating adapter
        sAdapter = new SimpleAdapter(this, data, R.layout.list_item_weather, from, to);

        // pick list and set adapter for it
        lvSimple = (ListView) findViewById(R.id.listView);
        lvSimple.setAdapter(sAdapter);

        // dynamically create list items
        this.createList();
    }

    public void createList() {
        try {
            URL url = new URL(FORECAST_URL);
            WeatherReport report = WeatherHandler.getWeatherReport(url);

            /* Print location meta data */
            System.out.println(report);

            for (WeatherForecast forecast : report) {
                dateText = forecast.getStartYYMMDD();
                periodCode = forecast.getPeriodCode();
                temperatureText = forecast.getTemp() + getString(R.string.VW_TEMPTEXT);
                weatherCode = forecast.getWeatherCode();
                periodText = forecast.getStartHHMM() + "-" + forecast.getEndHHMM();

                // creating new Map
                map = new HashMap<String, Object>();
                map.put(ATTRIBUTE_NAME_DATE_TEXT, dateText);
                map.put(ATTRIBUTE_NAME_PERIOD_TEXT, periodText);
                map.put(ATTRIBUTE_NAME_TEMP_TEXT, temperatureText);
                map.put(ATTRIBUTE_NAME_WEATHER_IMAGE, showImage(weatherCode, periodCode));

                // adding it into collection
                data.add(map);

                // notify about data changing
                sAdapter.notifyDataSetChanged();
            }
        } catch (NoInternetConnectionException nice) {
            Toast.makeText(this, R.string.VM_NO_INTERNET, Toast.LENGTH_SHORT).show();
        } catch (Exception ioe) {
            throw new RuntimeException(ioe.getLocalizedMessage());
        }
    }

    // image for showing according to period
    public int showImage(int weatherValue, int periodValue) {
        switch (weatherValue) {
            case 1: {
                if ((periodValue == 3) || (periodValue == 0)) {
                    return R.drawable.weather_01n;
                } else
                    return R.drawable.weather_01d;
            }
            case 2: {
                if ((periodValue == 3) || (periodValue == 0)) {
                    return R.drawable.weather_02n;
                } else
                    return R.drawable.weather_02d;
            }
            case 3: {
                if ((periodValue == 3) || (periodValue == 0)) {
                    return R.drawable.weather_03n;
                } else
                    return R.drawable.weather_03d;
            }
            case 4:
                return R.drawable.weather_04;
            case 5: {
                if ((periodValue == 3) || (periodValue == 0)) {
                    return R.drawable.weather_05n;
                } else
                    return R.drawable.weather_05d;
            }
            case 6: {
                if ((periodValue == 3) || (periodValue == 0)) {
                    return R.drawable.weather_06n;
                } else
                    return R.drawable.weather_06d;
            }
            case 7: {
                if ((periodValue == 3) || (periodValue == 0)) {
                    return R.drawable.weather_07n;
                } else
                    return R.drawable.weather_07d;
            }
            case 8: {
                if ((periodValue == 3) || (periodValue == 0)) {
                    return R.drawable.weather_08n;
                } else
                    return R.drawable.weather_08d;
            }
            case 9:
                return R.drawable.weather_9;
            case 10:
                return R.drawable.weather_10;
            case 11:
                return R.drawable.weather_11;
            case 12:
                return R.drawable.weather_12;
            case 13:
                return R.drawable.weather_13;
            case 14:
                return R.drawable.weather_14;
            case 15:
                return R.drawable.weather_15;
            default:
                return R.drawable.weather_0;
        }
    }
}
