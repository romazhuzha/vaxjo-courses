package dv606.rz222ap.assignment1;

class NoInternetConnectionException extends Exception {
    /*
     * This Exceptions is used for the showing toast about not having available
     * internet connection.
     */
    private static final long serialVersionUID = 7713553643429945978L;

    // Parameterless Constructor
    public NoInternetConnectionException() {
    }

    // Constructor that accepts a message
    public NoInternetConnectionException(String message) {
        super(message);
    }

    public NoInternetConnectionException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
