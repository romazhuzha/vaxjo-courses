package dv606.rz222ap.assignment1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity {
    private List<String> activitiesList = new ArrayList<>();
    private final List<Class> classesList = new ArrayList<Class>() {{
        add(Random.class);
        add(BMI.class);
        add(MyCountries.class);
        add(VaxjoWeather.class);
    }};

    @SuppressWarnings("rawtypes")
    private final Map<String, Class> activityClassMap = new HashMap<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* Add Activities to list */
        activitiesList = Arrays.asList(getResources().getStringArray(R.array.EX_NAMES));

        Iterator<String> activitiesIterator = activitiesList.iterator();
        Iterator<Class> classesIterator = classesList.iterator();
        while (activitiesIterator.hasNext() || classesIterator.hasNext()) {
            activityClassMap.put(activitiesIterator.next(), classesIterator.next());
        }

        setListAdapter(new ArrayAdapter<>(this, R.layout.list_item_main, activitiesList));

        /* Attach list item listener */
        ListView lv = getListView();
        lv.setOnItemClickListener(new OnItemClick());
    }

    /* Private Help Entities */
    private class OnItemClick implements OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /* Find selected activity */
            Class<?> activity_class = activityClassMap.get(activitiesList.get(position));
            /* Start new Activity */
            Intent intent = new Intent(MainActivity.this, activity_class);
            MainActivity.this.startActivity(intent);
        }
    }
}
