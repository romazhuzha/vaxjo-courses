package dv606.rz222ap.assignment1;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class BMI extends Activity implements OnClickListener {
    // needful outlines
    private static EditText enteredHeight, enteredWeight;
    private static TextView valueBMI, categoryBMI;

    // some help strings
    private static final String FORMAT_BMI = "###.#";

    private static final DecimalFormat df = new DecimalFormat(FORMAT_BMI);

    private static String BMIValue;

    // Keys for saving instance state
    private static final String BMI_VALUE = "valueBMI";
    private static final String BMI_CATEGORY = "categoryBMI";

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        // preparing outlines
        enteredHeight = (EditText) findViewById(R.id.editHeight);
        enteredWeight = (EditText) findViewById(R.id.editWeight);
        valueBMI = (TextView) findViewById(R.id.valueBMI);
        categoryBMI = (TextView) findViewById(R.id.categoryBMI);
        Button computeButton = (Button) findViewById(R.id.computeButton);
        Button resetButton = (Button) findViewById(R.id.resetButton);

        computeButton.setOnClickListener(this);
        resetButton.setOnClickListener(this);

        String BMICategory;
        if (savedInstanceState != null) {
            BMIValue = savedInstanceState.getString(BMI_VALUE);
            BMICategory = savedInstanceState.getString(BMI_CATEGORY);
        } else {
            BMIValue = getString(R.string.BMI_VALUE);
            BMICategory = getString(R.string.BMI_CATEGORY);
        }
        valueBMI.setText(BMIValue);
        categoryBMI.setText(BMICategory);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BMI_VALUE, valueBMI.getText().toString());
        outState.putString(BMI_CATEGORY, categoryBMI.getText().toString());
    }

    private void computeBMI() throws NumberFormatException {
        try {
            // Checking for empty fields
            if (enteredWeight.length() == 0) {
                throw new WrongInputDataException(
                        getString(R.string.BMI_ERROR_EMPTY_WEIGHT));
            } else if (enteredHeight.length() == 0) {
                throw new WrongInputDataException(
                        getString(R.string.BMI_ERROR_EMPTY_HEIGHT));
            }
            String enteredWeightString = enteredWeight.getText().toString();
            String enteredHeightString = enteredHeight.getText().toString();

            double weight = Double.valueOf(enteredWeightString);
            double height = Double.valueOf(enteredHeightString);

            // Entered data validating
            if ((weight > 350) || (weight < 30)) {
                throw new WrongInputDataException(
                        getString(R.string.BMI_ERROR_WRONG_WEIGHT));
            } else if ((height > 250) || (height < 50)) {
                throw new WrongInputDataException(
                        getString(R.string.BMI_ERROR_WRONG_HEIGHT));
            }

            // Computing BMI value
            double valueBMITemp = weight / (height * height / 10000);
            BMIValue = df.format(valueBMITemp);
            valueBMI.setText(BMIValue);

            if (valueBMITemp < 15) {
                categoryBMI.setText(R.string.BMI_CAT_1);
            } else if (valueBMITemp < 16) {
                categoryBMI.setText(R.string.BMI_CAT_2);
            } else if (valueBMITemp < 18.5) {
                categoryBMI.setText(R.string.BMI_CAT_3);
            } else if (valueBMITemp < 25) {
                categoryBMI.setText(R.string.BMI_CAT_4);
            } else if (valueBMITemp < 30) {
                categoryBMI.setText(R.string.BMI_CAT_5);
            } else if (valueBMITemp < 35) {
                categoryBMI.setText(R.string.BMI_CAT_6);
            } else if (valueBMITemp < 40) {
                categoryBMI.setText(R.string.BMI_CAT_7);
            } else {
                categoryBMI.setText(R.string.BMI_CAT_8);
            }
        } catch (WrongInputDataException wid) {
            Toast.makeText(this, wid.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void resetView() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();

        overridePendingTransition(0, 0);
        startActivity(getIntent());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.resetButton:
            this.resetView();
            break;
        case R.id.computeButton:
            this.computeBMI();
            break;
        default:
            break;
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        super.onKeyUp(keyCode, event);
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            this.computeBMI();
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }
}
