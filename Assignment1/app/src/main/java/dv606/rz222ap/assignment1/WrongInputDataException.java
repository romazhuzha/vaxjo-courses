package dv606.rz222ap.assignment1;

class WrongInputDataException extends RuntimeException {
    /**
     * This Exceptions is used for the showing toastes with error messages.
     * Should be throwed and catched.
     */
    private static final long serialVersionUID = -3285946593902083954L;

    // Parameterless Constructor
    public WrongInputDataException() {
    }

    // Constructor that accepts a message
    public WrongInputDataException(String message) {
        super(message);
    }
}
