package dv606.rz222ap.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

public class AddInformation extends Activity {

    // constants strings for extra messages
    private static final String EXTRA_MESSAGE_YEAR = "MESSAGE_YEAR";
    private static final String EXTRA_MESSAGE_COUNTRY = "MESSAGE_COUNTRY";

    // needful outlines
    private static EditText editYear, editCountry;

    // reply to MyCountries activity
    private static final Intent reply = new Intent();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_information);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        editYear = (EditText) findViewById(R.id.editYear);
        editCountry = (EditText) findViewById(R.id.editCountry);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_add_information, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED, reply);
                finish();
                break;

            // if 'SAVE' button is pressed
            case R.id.saveEntered:
                this.saveEntries();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        super.onKeyUp(keyCode, event);
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            this.saveEntries();
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    private void saveEntries() {
        try {
            // entered data validating
            if (editYear.length() != 4) {
                throw new WrongInputDataException(getString(R.string.MC_ERROR_EMPTY_YEAR));
            } else if ((editCountry.length() == 0) || (editCountry.length() > 75)) {
                throw new WrongInputDataException(getString(R.string.MC_ERROR_EMPTY_COUNTRY));
            }

            String year = editYear.getText().toString();
            String country = editCountry.getText().toString();

            if (!country.matches("^[a-zA-Z\\s\\-]+$")) {
                throw new WrongInputDataException(getString(R.string.MC_ERROR_WRONG_COUNTRY));
            }

            int yearValue = Integer.valueOf(year);

            if ((yearValue < 1900) || (yearValue > Calendar.getInstance().get(Calendar.YEAR))) {
                throw new WrongInputDataException(getString(R.string.MC_ERROR_WRONG_YEAR));
            }

            // put entered data in extra
            reply.putExtra(EXTRA_MESSAGE_YEAR, year);
            reply.putExtra(EXTRA_MESSAGE_COUNTRY, country);

            // send 'OK' and extras to super activity
            setResult(RESULT_OK, reply);
            finish();
        } catch (WrongInputDataException wide) {
            Toast.makeText(this, wide.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public String getExtraMessageYear() {
        return EXTRA_MESSAGE_YEAR;
    }

    public String getExtraMessageCountry() {
        return EXTRA_MESSAGE_COUNTRY;
    }
}
