# Links

- [Classroom](https://mymoodle.lnu.se/course/view.php?id=16323)
- Ideas list from [Workflowy](https://workflowy.com/s/tQ9I0x4ew9)
- Trello [board](https://trello.com/b/Whn35qOs)(in process of migrating from Workflowy)
- [Ship.io CI board](https://app.ship.io/dashboard#/jobs/10291/history)